package com.mytest.demoapp;

import android.widget.Button;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.annotation.Config;

import androidx.fragment.app.testing.FragmentScenario;
import androidx.test.runner.AndroidJUnit4;

import static androidx.fragment.app.testing.FragmentScenario.launchInContainer;
import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.assertion.ViewAssertions.doesNotExist;
import static androidx.test.espresso.assertion.ViewAssertions.matches;
import static androidx.test.espresso.matcher.ViewMatchers.isDisplayed;
import static androidx.test.espresso.matcher.ViewMatchers.withId;
import static androidx.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.Matchers.allOf;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

@RunWith(AndroidJUnit4.class)
@Config(manifest = Config.NONE)
public class ErrorDialogTest {

    private FragmentScenario<ErrorDialog> mScenario;

    @Before
    public void setup() {
        mScenario = launchInContainer(ErrorDialog.class);
    }

    @Test
    public void create_view() {
        mScenario.onFragment(dialog -> {
            assertNotNull(dialog);
            assertNotNull(dialog.getView());
            assertTrue(dialog.isCancelable());
            assertNotNull(dialog.mDismissBtn);
            // assertFalse(dialog.isCancelable());

            onView(withId(R.id.dialog_error_title))
                    .check(matches(allOf(
                            withText(R.string.error_title)
                            //, isDisplayed()
                    )));

            onView(withId(R.id.dialog_error_description))
                    .check(matches(allOf(
                            withText(R.string.error_description)
                            //, isDisplayed()
                    )));

            onView(withId(R.id.dialog_error_dismiss_button))
                    .check(matches(allOf(
                            withText(R.string.button_dismiss)
                            //, isDisplayed()
                    )));
        });
    }

    @Test
    public void dismiss() {
        mScenario.onFragment(dialog -> {
            Button btn = dialog.mDismissBtn;
            btn.performClick();

            onView(withId(R.id.dialog_error_title))
                    .check(doesNotExist());
        });
    }
}


