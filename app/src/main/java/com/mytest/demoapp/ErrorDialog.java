package com.mytest.demoapp;

import android.app.Dialog;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatDialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ErrorDialog extends AppCompatDialogFragment {

    @BindView(R.id.dialog_error_dismiss_button)
    Button mDismissBtn;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = populateUI(inflater);
        mDismissBtn.setOnClickListener(ignore -> dismiss());
        // FIXME: getDialog causes NPE in tests
        Dialog dialog = getDialog();
        System.out.println("Dialog: " + dialog);
        dialog.setCanceledOnTouchOutside(false);

        return view;
    }

    private View populateUI(LayoutInflater inflater) {
        View view = inflater.inflate(R.layout.error_dialog, null);
        ButterKnife.bind(this, view);
        return view;
    }
}
