package com.mytest.demoapp;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;
import butterknife.BindView;
import butterknife.ButterKnife;

import android.os.Bundle;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    @BindView(R.id.activity_main_press_me)
    TextView mPressMe;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        mPressMe.setOnClickListener(ignore -> showErrorDialog());
    }

    private void showErrorDialog() {
        ErrorDialog dialog = new ErrorDialog();
        dialog.show(getSupportFragmentManager(), "TAG_ERROR_DIALOG");
    }
}
